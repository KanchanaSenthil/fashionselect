Feature: search


Scenario Outline: search with valid product name
Given I am in Home Page
When I search for a valid product name "<searchWord>"
Then I should see search results page

Examples: 
|searchWord|
|jeans|
|shirt|
|shoe|

@Kanchana
Scenario: search with invalid product name
Given I am in Home Page
When I search for a invalid product name "gdhfd"
Then I should see no search results found page

Scenario: search with empty product name
Given I am in Home Page
When I search for a empty product name " "
Then I should see no search results found page

Scenario: search with capital letters
Given I am in Home Page
When I search for a product in capital letters "TOYS"
Then I should see search results page

