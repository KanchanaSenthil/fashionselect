package Runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)						 
@CucumberOptions(
		glue="StepDefinition", 
		features = "src/test/resources/feature", tags="@Kanchana") 

public class RunnerClass {

}
