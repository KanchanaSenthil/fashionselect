package StepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class AllStepDefinition {

	public static WebDriver driver;
	
	@Given("^I am in Home Page$")
	public void i_am_in_Home_Page() throws Throwable {
		System.setProperty("webdriver.chrome.driver",
		"C:\\\\Users\\\\User\\\\Desktop\\\\Automation Testing - Raghu\\\\Java & Eclipse Sw's\\\\Selenium Automation\\\\chromedriver_win32\\\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.selectfashion.co.uk/");
		Assert.assertEquals("Women's Clothing | Online Shopping | Fashion Clothes UK | SELECT", driver.getTitle());
		//comment
	}

	@When("^I search for a valid product name \"([^\"]*)\"$")
	public void i_search_for_a_valid_product_name(String searchWord) throws Throwable {
		driver.findElement(By.cssSelector("#productsearch")).clear();
		driver.findElement(By.cssSelector("#productsearch")).sendKeys(searchWord);
		driver.findElements(By.cssSelector(".btn-default.btn-lg")).get(0).click();
		Assert.assertEquals("Product Results for ".toLowerCase()+searchWord.toLowerCase(), driver.findElement(By.cssSelector("strong")).getText().toLowerCase());
		
	}

	@Then("^I should see search results page$")
	public void i_should_see_search_results_page() throws Throwable {
		driver.quit();
	}

	@When("^I search for a invalid product name \"([^\"]*)\"$")
	public void i_search_for_a_invalid_product_name(String invalidProdName) throws Throwable {
		driver.findElement(By.cssSelector("#productsearch")).clear();
		driver.findElement(By.cssSelector("#productsearch")).sendKeys(invalidProdName);
		driver.findElements(By.cssSelector(".btn-default.btn-lg")).get(0).click();
		Assert.assertEquals("WE DID NOT FIND ANY RESULTS FOR ".toLowerCase()+invalidProdName.toLowerCase(), driver.findElement(By.xpath(".//div[@class='infobox notfound']//p")).getText().toLowerCase());
	}

	@Then("^I should see no search results found page$")
	public void i_should_see_no_search_results_found_page() throws Throwable {
		driver.quit();
	}

	@When("^I search for a empty product name$")
	public void i_search_for_a_empty_product_name() throws Throwable {
		driver.findElement(By.cssSelector("#productsearch")).clear();
		driver.findElement(By.cssSelector("#productsearch")).sendKeys(" ");
		driver.findElements(By.cssSelector(".btn-default.btn-lg")).get(0).click();  
	}
	
	@When("^I search for a product in capital letters$")
	public void i_search_for_a_product_in_capital_letters() throws Throwable {
		driver.findElement(By.cssSelector("#productsearch")).clear();
		driver.findElement(By.cssSelector("#productsearch")).sendKeys("JEANS");
		driver.findElements(By.cssSelector(".btn-default.btn-lg")).get(0).click();
		
		System.out.println("Kanchana Project");
	}

}
